diaper_krystal_body_1
{
	dpreflectcube cubemaps/default/sky
 	{
		map textures/diaper_krystal_body_1.tga
		rgbgen lightingDiffuse
	}
}

diaper_krystal_body_0
{
	dpreflectcube cubemaps/default/sky
 	{
		map textures/diaper_krystal_body_0.tga
		rgbgen lightingDiffuse
	}
}


diaper_krystal_eyes
{
	dpreflectcube cubemaps/default/sky
 	{
		map textures/diaper_krystal_eyes.tga
		rgbgen lightingDiffuse
	}
}

diaper_krystal_metal
{
	dpreflectcube cubemaps/default/sky
 	{
		map textures/diaper_krystal_metal.tga
		rgbgen lightingDiffuse
	}
}
