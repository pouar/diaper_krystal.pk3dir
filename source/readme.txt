Diapered Krystal for Xonotic, based off of http://dl.xonotic.co/xmdl-krystal_v1.1.pk3
normal map for the diaper is based off of https://cc0textures.com/view.php?tex=Fabric01
some textures backported from the 2.7 model by Mikiel2171

repo along with the blend and xcf files is here since the blend and xcf files were kinda big https://gitlab.com/pouar/diaper_krystal.pk3dir

Original readme.txt

This package contains the Krystal player model for Xonotic.
The model was created by Mikiel2171 and released for free use. http://mikiel2171.deviantart.com/art/Krystal-release-for-Blender-279184887
Ported to Xonotic by MirceaKitsune.
Krystal belongs to Nintendo and is part of the Starfox series.
The preview image was created by Hayakain. http://hayakain.deviantart.com/art/Krystal-Fox-from-above-251896924
